pragma solidity ^0.4.19;

import "../node_modules/zeppelin-solidity/contracts/math/SafeMath.sol";

contract ethTermBets {

    struct BetStruct {
        address maker; 
        address friend;
        uint ID;
        uint amount;
        string description;
    }

    uint numBets;
    // Map betID to a BetStruct
    mapping (uint => BetStruct) bets;
    // Map Bet ID to Creator address
    mapping (uint => address) betToCreator;
    // Map Address to number of bets
    mapping (address => uint) creatorBetCount;

    // Creates a new bet between two Public addresses
    function newBet(
        address makerAddress,
        address friendAddress,
        uint betAmount,
        string betDescription
    )
    public returns (uint betID) {
        betID = numBets++; // betID is return variable
        betToCreator[betID] = msg.sender; // Map the betID to sender Address
        creatorBetCount[msg.sender]++; // Incriment the number of bets the creator has
        // Creates new struct and saves in storage. We leave out the mapping type.
        bets[betID] = BetStruct(
            makerAddress,
            friendAddress,
            betID,
            betAmount,
            betDescription
        );
    }
}
