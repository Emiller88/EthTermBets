var Migrations = artifacts.require("./Migrations.sol");
var Bets = artifacts.require("./EthTermBets.sol");

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(Bets);
};
